package com.example.jpmg.modelo;

public interface Comunicador {
    public void responder(String datos);
}
