package com.example.progra;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class activity_actividad_swalumnos extends AppCompatActivity implements View.OnClickListener {
    EditText cajaId, cajaNombres, CajaDireccion;
    Button botonGuardar, botonModificar, botonEliminar, botonBuscarTodos, botonBuscarID;
    TextView datos;

    //definir las URL del servicio web
    String host = "http://reneguaman.000webhostapp.com/obtener_alumnos.php";
    String insert = "/insertar_alumnos.php";
    String get = "/obtener_alumnos.php";
    String getById = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";
    //clase interna
    ServicioWeb sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swalumnos);
        cargarComponentes();
    }

    private void cargarComponentes() {
        cajaId = findViewById(R.id.txtSWid);
        cajaNombres = findViewById(R.id.txtSWnombre);
        CajaDireccion = findViewById(R.id.txtswDir);
        botonGuardar = findViewById(R.id.btnCrearSW);
        botonModificar = findViewById(R.id.btnModificarSW);
        botonEliminar = findViewById(R.id.btnEliminarSW);
        botonBuscarTodos = findViewById(R.id.btnBuscarSW);
        botonBuscarID = findViewById(R.id.btnBuscarIDSW);
        datos = findViewById(R.id.datosSW);

        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonBuscarID.setOnClickListener(this);
        botonBuscarTodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        sw = new ServicioWeb();
        switch (view.getId()){
            case R.id.btnCrearSW:
            break;
            case R.id.btnBuscarSW:
                sw.execute(host.concat(get),"1");
                break;
        }
    }


    //acceder al servicio web mediante un hilo
    class ServicioWeb extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... parametros) {
            String consulta = "";
            URL url = null;
            String ruta = parametros[0]; //esta es la ruta .. "http://reneguaman.000webhostapp.com/obtener_alumnos.php"
            //0 host
            //1 tipo de operacion
            if (parametros[1].equals("1")) {
                try {
                    url = new URL(ruta);
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    int codigoRespuesta = conexion.getResponseCode();
                    if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                        InputStream in = new BufferedInputStream(conexion.getInputStream());
                        BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                        consulta += lector.readLine();
                        Log.e("mensaje", consulta);
                    }
                } catch (Exception ex) {

                }

            }
            return consulta;
        }

     //definir datos que se le envian

        @Override
        protected void onPostExecute(String s) {
            datos.setText(s);
            ///super.onPostExecute(s);
        }
    }
}

